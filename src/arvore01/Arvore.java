package arvore01;

import java.util.Random;
import java.util.Scanner;

public class Arvore {

//	vamos colocar 10 numeros em 1 arvore
//	consultar n�meros pares

//	declarar a minha arvore
	private static class ARVORE {
		public int numero;
		public ARVORE direita, esquerda;
	}

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		// intanciar a arvore
		ARVORE raiz = null;
		int opcao, achou;

		do {
			System.out.println("\nOpera��es com uma �rvore");
			System.out.println("1- Preencher a �rvore");
			System.out.println("2- Imprimir os pares");
			System.out.println("3- Imprimir os impares");
			System.out.println("4- Imprimir primos");
			System.out.println("5- Consultar a arvore em ordem");
			System.out.println("6- Consultar a arvore em pr�-ordem");
			System.out.println("7- Consultar a arvore em p�s-ordem");
			System.out.println("8- sair");

			System.out.print("Escolha uma op��o: ");
			opcao = sc.nextInt();

			// vamos tratar os poss�veis erros
			if (opcao < 1 || opcao > 8) {
				System.out.println("Op��o inv�lida");
			} else if (opcao == 1) {
				// n�s vamos preencher a �rvore
				raiz = null;
				for (int i = 1; i <= 10; i++) {
//					System.out.print("Informe o n�mero a ser inserido: ");
//					int numero = sc.nextInt();
					Random g = new Random();
//					raiz = inserir(raiz, i-1);
					raiz = inserir(raiz, g.nextInt(25));
				}
			} else if (opcao == 2) {
				// vamos imprimir os pares
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					achou = 0;
					achou = consultarPares(raiz, achou);
					if (achou == 0) {
						System.out.println("N�o h� pares!");
					}
				}
			} else if (opcao == 3) {
				// consultar impares
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					achou = 0;
					achou = consultarImpares(raiz, achou);
					if (achou == 0) {
						System.out.println("N�o h� �mpares!");
					}
				}
			} else if (opcao == 4) {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					achou = 0;
					achou = consultarPrimos(raiz, achou);
					if (achou == 0) {
						System.out.println("N�o h� primos!");
					}
				}
			} else if (opcao == 5) {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore s�o: ");
					// chamar m�todo de consultar
					consultarEmOrdem(raiz);
				}
			} else if (opcao == 6) {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore em pr�-ordem s�o: ");
					// chamar m�todo de consultar
					consultarPreOrdem(raiz);
				}
			}
		} while (opcao != 8);
	}

	// m�todo para inserir �rvore
	public static ARVORE inserir(ARVORE aux, int numero) {
		// primeiro passo: sera que a arvore n�o tem dado
		if (aux == null) {
			aux = new ARVORE();
			aux.numero = numero;
			aux.direita = null;
			aux.esquerda = null;
		} else if (numero < aux.numero) {
			aux.esquerda = inserir(aux.esquerda, numero);
		} else {
			aux.direita = inserir(aux.direita, numero);
		}
		return aux;
	}

	// m�todo que consulta os n�meros pares de uma �rvore
	public static int consultarPares(ARVORE aux, int achou) {
		if (aux != null) {
			if (aux.numero % 2 == 0) {
				System.out.println("Os n�meros pares s�o: " + aux.numero + " ");
				achou = 1;
			}
			achou = consultarPares(aux.esquerda, achou);
			achou = consultarPares(aux.direita, achou);
		}
		return achou;
	}

	// m�todo que consulta os n�meros �mpares de uma �rvore
	public static int consultarImpares(ARVORE aux, int achou) {
		if (aux != null) {
			if (aux.numero % 3 == 0) {
				System.out.println("Os n�meros �mpares s�o: " + aux.numero + " ");
				achou = 1;
			}
			achou = consultarImpares(aux.esquerda, achou);
			achou = consultarImpares(aux.direita, achou);
		}
		return achou;
	}

	public static int consultarPrimos(ARVORE aux, int achou) {
		if (aux != null) {
			int cont = 0;
			for (int i = 2; i <= aux.numero; i++) {
				if (aux.numero % i == 0) {
					cont++;
				}
			}
			if (cont == 1) {
				System.out.println("N�mero primo: " + aux.numero);
				achou = 1;
			}
			achou = consultarPrimos(aux.esquerda, achou);
			achou = consultarPrimos(aux.direita, achou);
		}
		return achou;
	}

	public static void consultarEmOrdem(ARVORE aux) {
		if (aux != null) {
			consultarEmOrdem(aux.esquerda);
			System.out.println("N�mero: " + aux.numero + " ");
			consultarEmOrdem(aux.direita);
		}
	}

	public static void consultarPreOrdem(ARVORE aux) {
		if (aux != null) {
			System.out.println("N�mero: " + aux.numero);
			consultarPreOrdem(aux.esquerda);
			consultarPreOrdem(aux.direita);
		}
	}

	public static void consultarPosOrdem(ARVORE aux) {
		if (aux != null) {
			consultarPreOrdem(aux.esquerda);
			consultarPreOrdem(aux.direita);
			System.out.println("N�mero: " + aux.numero);
		}
	}

}
