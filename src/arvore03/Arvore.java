package arvore03;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class Nodo {
	public int item;
	public Nodo dir;
	public Nodo esq;
}

public class Arvore {
	private Nodo raiz;

	public Arvore() {
		raiz = null;
	}

	public void inserir(Integer valor) {
		Nodo aux = new Nodo();
		aux.item = valor;
		aux.dir = null;
		aux.esq = null;

		if (raiz == null)
			raiz = aux;
		else {
			Nodo atual = raiz;
			Nodo anterior;
			while (true) {
				anterior = atual;
				if (valor <= atual.item) {
					atual = atual.esq;
					if (atual == null) {
						anterior.esq = aux;
						return;
					}
				} else {
					atual = atual.dir;
					if (atual == null) {
						anterior.dir = aux;
						return;
					}
				}
			} // fim do while
		}
	} // fim do inserir

	public void inOrder(Nodo atual) {
		if (atual != null) {
			inOrder(atual.esq);
			System.out.print(atual.item + " ");
			inOrder(atual.dir);
		}
	}

	public void walkInOrder() {
		System.out.print("\n Exibindo em ordem: ");
		inOrder(raiz);
		System.out.println("");
	}

	private Stack<Integer> pilha = new Stack<>();

	private void consultarPares(Nodo aux) {
		if (aux != null) {
			if (aux.item % 2 == 0) {
				pilha.add(aux.item);
			}
			consultarPares(aux.esq);
			consultarPares(aux.dir);
		}
	}

	public void exibirPilha() {
		consultarPares(raiz);
		for (Integer integer : pilha) {
			System.out.println("  Valor da pilha: " + integer);
		}
	}

	private Queue<Integer> fila = new LinkedList<>();
	
	private void consultarImpares(Nodo aux) {
		if (aux != null) {
			if (aux.item % 2 != 0) {
				fila.add(aux.item);
			}
			consultarImpares(aux.esq);
			consultarImpares(aux.dir);
		}
	}
	
	public void exibirFila() {
		consultarImpares(raiz);
		for (Integer integer : fila) {
			System.out.println("  Valor da fila: " + integer);
		}
	}
}
