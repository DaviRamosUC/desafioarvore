package arvore03;

import java.util.Random;
import java.util.Scanner;

public class Main {

	public static Scanner sc = new Scanner(System.in);

	@SuppressWarnings("null")
	public static void main(String[] args) {

		Arvore raiz = new Arvore();
		int opcao;

		do {
			System.out.println("\nOpera��es com uma �rvore");
			System.out.println("1- Preencher a �rvore");
			System.out.println("2- Consultar a �rvore em ordem");
			System.out.println("3- Consultar valores pares da �rvore");
			System.out.println("4- Consultar valores impares da �rvore");
			System.out.println("5- sair");
			System.out.print("Escolha uma op��o: ");
			opcao = sc.nextInt();

			switch (opcao) {
			case 1: {
				for (int i = 0; i < 10; i++) {
					Random g = new Random();
//					System.out.println("Informe o n�mero a ser inserido: ");
//					int numero = sc.nextInt();
//					raiz = inserir(raiz, numero);
					raiz.inserir(g.nextInt(25));
				}
				System.out.println("N�meros cadastrados com sucesso!");
				break;
			}
			case 2: {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore s�o: ");
					raiz.walkInOrder();
				}

				break;
			}
			case 3: {
				System.out.println(" Valores pares da �rvore:");
				raiz.exibirPilha();
				break;
			}
			case 4: {
				System.out.println(" Valores impares da �rvore:");
				raiz.exibirFila();
				break;
			}
			case 5: {
				System.out.println("At� a pr�xima!");
				System.exit(0);
				break;
			}

			}
		} while (true);

	}
}
