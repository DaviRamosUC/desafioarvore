package arvore02;

import java.util.Random;
import java.util.Scanner;

public class Arvore {

//	declarar a minha arvore
	private static class ARVORE {
		public int numero;
		public ARVORE direita, esquerda;
	}

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		// intanciar a arvore
		ARVORE raiz = null;
		int opcao;

		do {
			System.out.println("\nOpera��es com uma �rvore");
			System.out.println("1- Preencher a �rvore");
			System.out.println("2- Consultar a arvore em ordem");
			System.out.println("3- Consultar a arvore em pr�-ordem");
			System.out.println("4- Consultar a arvore em p�s-ordem");
			System.out.println("5- sair");
			System.out.print("Escolha uma op��o: ");
			opcao = sc.nextInt();

			switch (opcao) {
			case 1: {
				for (int i = 0; i < 10; i++) {
					Random g = new Random();
//					System.out.println("Informe o n�mero a ser inserido: ");
//					int numero = sc.nextInt();
//					raiz = inserir(raiz, numero);
					raiz = inserir(raiz, g.nextInt(25));
				}
				System.out.println("N�meros cadastrados com sucesso!");
				break;
			}
			case 2: {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore s�o: ");
					consultarEmOrdem(raiz);
				}

				break;
			}
			case 3: {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore em pr�-ordem s�o: ");
					consultarPreOrdem(raiz);
				}
				break;
			}
			case 4: {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher");
				} else {
					System.out.println("Os n�meros da �rvore em p�s-ordem s�o: ");
					consultarPosOrdem(raiz);
				}
				break;
			}
			case 5: {
				System.out.println("At� a pr�xima!");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Op��o n�o cadastrada!");
			}

		} while (true);

	}

	private static void consultarPreOrdem(ARVORE aux) {
		if (aux != null) {
			System.out.println("N�mero: " + aux.numero + " ");
			consultarPreOrdem(aux.esquerda);
			consultarPreOrdem(aux.direita);
		}
		
	}

	private static void consultarPosOrdem(ARVORE aux) {
		if (aux != null) {
			consultarPosOrdem(aux.esquerda);
			consultarPosOrdem(aux.direita);
			System.out.println("N�mero: " + aux.numero + " ");
		}
		
	}

	private static void consultarEmOrdem(ARVORE aux) {
		if (aux != null) {
			consultarEmOrdem(aux.esquerda);
			System.out.println("N�mero: " + aux.numero + " ");
			consultarEmOrdem(aux.direita);
		}

	}

	private static ARVORE inserir(ARVORE aux, int numero) {
		if (aux == null) {
			aux = new ARVORE();
			aux.numero = (numero * 100);
			aux.direita = null;
			aux.esquerda = null;
		} else if (numero < aux.numero) {
			aux.esquerda = inserir(aux.esquerda, numero);
		} else {
			aux.direita = inserir(aux.direita, numero);
		}
		return aux;
	}

}
